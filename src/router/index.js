import { createRouter, createWebHistory } from "vue-router";

import Home from "@/views/home/Home.vue";
import Profile from "@/views/profile/Profile.vue";
import Login from "@/views/login/Login.vue";
import ResetPass from "@/views/resetPass/ResetPass.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: Home,
  },
  {
    path: "/profile",
    name: "profile",
    component: Profile,
  },
  {
    path: "/login",
    name: "login",
    component: Login,
  },
  {
    path: "/reset-password",
    name: "resetPass",
    component: ResetPass,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.VUE_APP_SITE_URL),
  routes,
});

export default router;
