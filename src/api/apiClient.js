import axios from "axios";

const axiosClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

axiosClient.defaults.headers = {
  "Content-Type": "application/json",
  Accept: "application/json",
  authorization: localStorage.getItem("authToken"),
};

//All request will wait 2 seconds before timeout
axiosClient.defaults.timeout = 2000;

axiosClient.defaults.withCredentials = true;

export const get = (URL) => {
  return axiosClient.get(`${URL}`).then((response) => response);
};

export const post = (URL, payload) => {
  return axiosClient.post(`${URL}`, payload).then((response) => response);
};

export const patch = (URL, payload) => {
  return axiosClient.patch(`${URL}`, payload).then((response) => response);
};

export const _delete = (URL) => {
  return axiosClient.delete(`${URL}`).then((response) => response);
};

export const setAuthToken = (token) => {
  localStorage.setItem("authToken", token.replace("JWT ", ""));
  axiosClient.defaults.headers = {
    authorization: token.replace("JWT ", ""),
  };
};

export const clearToken = () => {
  localStorage.clear();
};

const apiClient = {
  post,
  get,
  patch,
  _delete,
  setAuthToken,
};

export { apiClient };
