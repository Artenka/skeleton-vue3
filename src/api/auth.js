import { apiClient } from "./apiClient";

export const getAuthStatus = () => {
  return apiClient.get("/auth/status");
};

export const userAuthorize = (payload) => {
  return apiClient.post("/auth", payload);
};
