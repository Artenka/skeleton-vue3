import { apiClient } from "./apiClient";

export const registration = (payload) => {
  return apiClient.post("/api/users/register", payload);
};
