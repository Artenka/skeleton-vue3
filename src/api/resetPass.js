import { apiClient } from "./apiClient";

export const resetPassword = (payload) => {
  return apiClient.post("/api/users/reset-password", payload);
};
