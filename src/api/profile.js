import { apiClient } from "./apiClient";

export const getProfile = () => {
  return apiClient.get("/api/users/profile");
};

export const patchProfile = (id, payload) => {
  return apiClient.patch(`/api/users/${id}`, payload);
};
